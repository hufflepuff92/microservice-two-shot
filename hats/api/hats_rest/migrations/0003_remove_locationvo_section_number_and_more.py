# Generated by Django 4.0.3 on 2022-12-05 04:12

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('hats_rest', '0002_hats_hat_name'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='locationvo',
            name='section_number',
        ),
        migrations.RemoveField(
            model_name='locationvo',
            name='shelf_number',
        ),
        migrations.AlterField(
            model_name='hats',
            name='location',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='hats', to='hats_rest.locationvo'),
        ),
    ]
