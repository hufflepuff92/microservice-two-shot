from django.core.exceptions import ObjectDoesNotExist
from django.db import models
from django.urls import reverse

# Create your models here.

class BinVO(models.Model):
    closet_name = models.CharField(max_length=100)
    import_href = models.CharField(max_length=200,unique = True)

    def __str__(self):
        return f"{self.closet_name}, {self.id}"


class Shoe(models.Model):
    shoe_nickname = models.CharField(max_length=200)
    manufacturer = models.CharField(max_length=200)
    model_name = models.CharField(max_length=200)
    color = models.CharField(max_length=100)
    picture_url = models.URLField(null=True)

    bin = models.ForeignKey(
        BinVO,
        related_name = "shoes",
        on_delete=models.CASCADE,
    )


    def __str__(self):
        return f"{self.id}, {self.shoe_nickname}"
