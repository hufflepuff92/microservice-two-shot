import { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';

const HatsList = () => {
  const [ hats, sethats ] = useState([])

  useEffect(()=> {
    const loadData = async () => {
      const url = 'http://localhost:8090/api/hats/';
      const response = await fetch(url);

      if (response.ok) {
        const data = await response.json();
        sethats(data.hats);
      } else {
        console.log("Error");
      }
    }

    loadData()

  }, [])

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Hats</h1>
          <table className="table">
            <thead>
                <th>ID</th>
                <th>Hat Name</th>
            </thead>
            <tbody>
                {
                  hats.map(c => <tr key={c.id}>
                      <td>{c.id}</td><td>{c.hat_name}</td>
                  </tr>)
                }
            </tbody>
          </table>
          <Link  to="/hats/new"><button className="btn btn-primary">Add A New Hat To Your Wardrobe</button></Link>
        </div>
      </div>
    </div>
  );
}

export default HatsList;
