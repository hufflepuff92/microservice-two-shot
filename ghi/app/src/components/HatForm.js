import {useEffect, useState} from "react";

const HatCreationForm = () => {
    const [formData, setFormData] = useState({
        hat_name: "",
        hat_fabric: "",
        hat_style_name: "",
        hat_color: "",
        hat_picture: "",
        location: [],
    })

    const [locations, setLocation] = useState([])

    useEffect(() => {
        const loadData = async () =>{
            const url = 'http://localhost:8100/api/locations/';
            const response = await fetch(url);

            if (response.ok) {
                const data = await response.json();
                setLocation(data.locations);
            } else {
                console.log("Error")
            }
        }
        loadData();
    }, [])

    const handleFormChange = (e) => {
        setFormData({
           ...formData,
            [e.target.name]: e.target.value,
        })
    }

    const handleSubmit = async (e) => {
        e.preventDefault();

        const url = 'http://localhost:8090/api/hats/';
        const fetchConfig = {
            method: 'POST',
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            const newhat = await response.json();

            setFormData({
                hat_name: '',
                hat_fabric: '',
                hat_style_name: '',
                hat_color: '',
                hat_picture: '',
                location: '',
            });
        }
    }

    return <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new Hat</h1>
          <form onSubmit={handleSubmit} id="create-hat-form">

            <div className="form-floating mb-3">
              <input onChange={handleFormChange} value={formData.hat_name} placeholder="hat_name" required type="text" name="hat_name" id="hat_name" className="form-control" />
              <label htmlFor="hat_name">Name of hat</label>
            </div>

            <div className="form-floating mb-3">
              <input onChange={handleFormChange} value={formData.hat_fabric} placeholder="hat_fabric" required type="text" name="hat_fabric" id="hat_fabric" className="form-control" />
              <label htmlFor="hat_fabric">Fabric of the hat</label>
            </div>

            <div className="form-floating mb-3">
              <input onChange={handleFormChange} value={formData.hat_style_name} placeholder="hat_style_name" required type="text" name="hat_style_name" id="hat_style_name" className="form-control" />
              <label htmlFor="hat_style_name">Style of the hat</label>
            </div>

            <div className="form-floating mb-3">
              <input onChange={handleFormChange} value={formData.hat_color} placeholder="hat_color" required type="text" name="hat_color" id="hat_color" className="form-control" />
              <label htmlFor="hat_color">Color of the hat</label>
            </div>

            <div className="form-floating mb-3">
              <input onChange={handleFormChange} value={formData.hat_picture} placeholder="picture url" required type="url" name="hat_picture" id="hat_picture" className="form-control" />
              <label htmlFor="hat_picture">URL for the picture of your hat</label>
            </div>

            <div className="mb-3">
              <select onChange={handleFormChange} value={formData.location} required name="location" id="location" className="form-select">
                <option value="">Choose a location</option>
                {locations.map(location => {
                  return (
                    <option key={location.id} value={location.id}>{location.closet_name}</option>
                  )
                })}
              </select>
            </div>

            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
}

export default HatCreationForm;
