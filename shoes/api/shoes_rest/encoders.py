from django.shortcuts import render
from .models import Shoe, BinVO
from common.json import ModelEncoder

# Create your views here.
class BinVOEncoder(ModelEncoder):
    model = BinVO
    properties = [
        "closet_name",
        "import_href",
    ]

class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "manufacturer",
        "model_name",
        "color",
        "picture_url",
    ]


class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "shoe_nickname",
        "manufacturer",
        "model_name",
        "color",
        "picture_url",
    ]
    encoders = {
        "bin": BinVOEncoder(),
    }
