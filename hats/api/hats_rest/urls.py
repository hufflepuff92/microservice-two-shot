from django.urls import path

from .views import api_hat_list, api_show_hat

urlpatterns = [
    path("hats/",api_hat_list, name="api_create_hats"),
    path("location/<int:location_vo_id>/hats/",api_hat_list, name="api_list_hats"),
    path("hats/<int:pk>/", api_show_hat, name="api_show_hat"),
]
