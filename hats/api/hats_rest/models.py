from django.db import models
from django.urls import reverse

# Create your models here.

class LocationVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    closet_name = models.CharField(max_length=100)

    def __str__(self):
        return f"{self.closet_name}, {self.id}"


class Hats(models.Model):
    hat_name = models.CharField(max_length=100)
    hat_fabric = models.CharField(max_length=100)
    hat_style_name = models.CharField(max_length=100)
    hat_color = models.CharField(max_length=100)
    hat_picture = models.URLField(null=True)

    location = models.ForeignKey(
        LocationVO,
        related_name = "hats",
        on_delete = models.CASCADE
    )

    def __str__(self):
        return f"{self.id}, {self.hat_name}"
