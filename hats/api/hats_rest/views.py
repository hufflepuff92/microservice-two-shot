from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse

import json
from common.json import ModelEncoder

from .models import LocationVO, Hats
# Create your views here.

class LocationVOEncoder(ModelEncoder):
    model = LocationVO
    properties = ["closet_name", "import_href"]

class Hat_ListEncoder(ModelEncoder):
    model = Hats
    properties = [
        "id",
        "hat_name",
        "hat_picture"
    ]

    def get_extra_data(self, o):
        return {"location": o.location.closet_name}

class Hat_DetailsEncoder(ModelEncoder):
    model = Hats
    properties = [
        "id"
        "hat_name",
        "hat_fabric",
        "hat_style_name",
        "hat_color",
        "hat_picture",
    ]
    encoders = {
        "location": LocationVOEncoder()
    }


@require_http_methods(["GET", "POST"])
def api_hat_list(request, location_vo_id=None):
    if request.method == "GET":
        if location_vo_id is not None:
            hats = Hats.objects.filter(location=location_vo_id)
        else:
            hats = Hats.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=Hat_ListEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            location_id = content['location']
            location = LocationVO.objects.get(id=location_id)
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse({"message": "invalid location id"}, status=400)

        hat = Hats.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=Hat_DetailsEncoder,
            safe=False,
        )

@require_http_methods(["GET", "DELETE"])
def api_show_hat(request, pk):
    if request.method == "GET":
        hat = Hats.objects.get(id=pk)
        return JsonResponse(
            hat,
            encoder=Hat_DetailsEncoder,
            safe=False,
        )
    else:
        count, _ = Hats.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
